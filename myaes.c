#include "myaes.h"

void addRoundKey(byte state[], byte words[], int a){
	for(int i=0; i<Nb; i++){
		for(int j=0; j<Nb; j++){
			state[4*i + j] = state[4*i + j] ^ words[4*(a+i) + j];
		}
	}	
}

void subBytes(byte state[]){
	for(int i=0; i<Nb; i++){
		for(int j=0; j<Nb; j++){
			state[4*i + j] = sbox[state[4*i + j] >> 4][state[4*i + j] & 0xf];
		}
	}
}

void shiftRows(byte state[]){
	byte row[4];
	for(int i=1; i<Nb; i++){
		for(int j=0; j<Nb; j++){
			row[j] = state[(4*j) + i];
		}
		for(int j=0; j<Nb; j++){
			state[(4*j) + i] = row[(j+i) % 4];
		}
	}
}

byte xor(byte state, byte x){
	return state ^= x;
}

byte mul(byte state, int x){
	byte a = state;
	byte msb = (unsigned char)((signed char)state >> 7);
	if(x == 2){
		if(msb == 0)
			return state << 1;
		else 
			return xor(state << 1, 0x1b);		
	} else {
		return xor(mul(state, 2), a);
	}
}

void mixColumns(byte state[]){
 	byte s0, s1, s2, s3;
 	
 	//ciclo sulle colonne
 	for(int i=0; i<Nb; i++){
 		s0 = state[i*4 + 0];
 		s1 = state[i*4 + 1];
 		s2 = state[i*4 + 2];
 		s3 = state[i*4 + 3];
 		
 	 	state[i*4 + 0] = xor(mul(s0, 2), xor(mul(s1, 3), xor(s2, s3)));
 	 	state[i*4 + 1] = xor(s0, xor(mul(s1, 2), xor(mul(s2, 3), s3)));
 	 	state[i*4 + 2] = xor(s0, xor(s1, xor(mul(s3, 3), mul(s2, 2))));
 	 	state[i*4 + 3] = xor(mul(s0, 3), xor(s1, xor(mul(s3, 2), s2)));
 	}
}

void subword(byte word[]){
	for(int i=0; i<Nb; i++){
		word[i] = sbox[word[i] >> 4][word[i] & 0xf];
	}
}


void rotword(byte word[]){
	byte temp[4];
	for(int i=0; i<Nb; i++){
		temp[i] = word[i];
	}
	for(int i=0; i<Nb; i++){
		word[i] = temp[(i+1) % Nb];
	}
}

void key_expansion(byte word[Nb*(Nr+1)]){
	byte temp[4];
	byte rconRound[4] = {0x00, 0x00, 0x00, 0x00};
	
	int i=0;
	
	//prime quattro words (0 - 3)
	while(i < Nk){
		for(int j=0; j<Nb; j++){
			word[i*4 + j] = chiave[i*4 + j];
		}
		i++; 
	}
	
		
	i = Nk; //4
	
	//restanti words (4 - 43)
	while(i < Nb*(Nr + 1) ){
		//temp = w[i-1]
		for(int j=0; j<Nb; j++){
			temp[j] = word[(i-1)*4 + j];
		}	
		
		if(i % Nk == 0){
			rotword(temp);
			subword(temp);
			
			rconRound[0] = rcon[i/Nk];
			
			//temp ^= rcon[i/Nk]; 
			for(int j=0; j<Nb; j++){
				temp[j] = temp[j] ^ rconRound[j];
			}
		} else if(Nk > 6 && (i % Nk == 4)) {
			subword(temp);
		}
		
		for(int j=0; j<Nb; j++){
			word[i*4 + j] = word[(i-Nk)*4 + j] ^ temp[j];
		}
		
		i++;
	}
}

void encryption(byte state[]){
	byte words[Nb*Nb*(Nr+1)] = {};		
	key_expansion(words);
	
	//primo round
	addRoundKey(state, words, 0);	
	
	//secondo - nono round
	for(int round=1; round < Nr; round++){
		subBytes(state);
		shiftRows(state);
		mixColumns(state);
		addRoundKey(state, words, round*Nb);
	}
	
	//ultimo round
	subBytes(state);
	shiftRows(state);
	addRoundKey(state, words, Nr*Nb);	
}

void setkey(uint8_t * key){
	for(int i=0; i<16 ; i++){
		chiave[i] = (byte)(key[i]);
	}
}

void crypto(uint8_t * input){
	encryption(input);
}
